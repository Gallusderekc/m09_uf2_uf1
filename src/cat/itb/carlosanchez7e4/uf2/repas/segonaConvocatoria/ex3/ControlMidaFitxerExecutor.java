package cat.itb.carlosanchez7e4.uf2.repas.segonaConvocatoria.ex3;

import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.concurrent.*;

public class ControlMidaFitxerExecutor {

    public static void main(String[] args) {
        ScheduledExecutorService temporitzador = Executors.newScheduledThreadPool(1);

        Scanner in = new Scanner(System.in);
        System.out.println("Fitxer?");
        String origen = in.next();

        Callable<Boolean> buscar = () -> {
            Path source = Paths.get(origen);
            boolean esta= Files.exists(source);
            long fileSize = Files.size(source);
            if(esta && fileSize <=1000) System.out.println("Ha aparegut el fitxer i es inferior a 1000 bytes");
            return esta;
        };

        ScheduledFuture<Boolean> q;
        try {
            do {
                q = temporitzador.schedule(buscar, 5, TimeUnit.SECONDS);
                //ALTRES FEINES DEL MAIN...
            } while (!q.get());
            temporitzador.shutdown();
        } catch (Exception e) {

        }
    }

}
