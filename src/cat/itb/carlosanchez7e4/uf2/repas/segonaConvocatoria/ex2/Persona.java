package cat.itb.carlosanchez7e4.uf2.repas.segonaConvocatoria.ex2;

import java.util.concurrent.TimeUnit;

public class Persona {
    String nom;
    int edat;

    public Persona(String nom, int edat) {
        this.nom = nom;
        this.edat = edat;
    }

    public String getNom() {
        return nom;
    }

    public int getEdat() {
        return edat;
    }
}
class Estudiant extends Persona{

    int nota;

    public Estudiant(String nom, int edat, int nota) {
        super(nom, edat);
        this.nota = nota;
    }
}

class Missatge extends Thread{
    Persona persona;
    Estudiant estudiant;
    @Override
    public void run() {
        try{
        System.out.println("Hola, soc " +
                persona.getNom() + " tinc " + persona.getEdat()
                + " y espero obtenir de nota un " + estudiant.nota + ".");
        TimeUnit.SECONDS.sleep(1);
    } catch (Exception e) {
    }
    }
}