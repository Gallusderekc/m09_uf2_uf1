package cat.itb.carlosanchez7e4.uf2.repas.segonaConvocatoria.ex1;

public class Estudiant implements Runnable {

    private String nom;
    private double notaMitjana;

    public Estudiant(String nom, double notaMitjana) {
        this.nom = nom;
        this.notaMitjana = notaMitjana;
    }



    @Override
    public void run() {
        System.out.println("l'estudiant " + nom + " ha tret una nota de " + notaMitjana + " de nota mitjana.");
    }

}
class Prova {

    public static void main(String[] args) {
        Thread t1=new Thread(new Estudiant("Alfons", 8.6));
        Thread t2=new Thread(new Estudiant("Gerard", 6.9));
        Thread t3=new Thread(new Estudiant("Francesc", 3.1));


        t1.start();
        t2.start();
        t3.start();

    }

}