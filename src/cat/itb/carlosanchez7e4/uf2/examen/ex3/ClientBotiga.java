package cat.itb.carlosanchez7e4.uf2.examen.ex3;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class ClientBotiga extends Thread{
    private Emprovador emp;

    public ClientBotiga(Emprovador e) {
        emp = e;
    }

    @Override
    public void run() {
        String[] roba = {"Camisa", "Jersei", "Pantaló", "Vestit", "Abric"};
        List<String> conjunt = Arrays.asList(roba);
        Collections.shuffle(conjunt);
        ThreadLocalRandom tlr = ThreadLocalRandom.current();
        conjunt = conjunt.subList(0, tlr.nextInt(1,conjunt.size()));
        emp.emprovarse(conjunt);
    }
}
