package cat.itb.carlosanchez7e4.uf2.examen.ex3;

import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Emprovador {
    private boolean ocupat = false;

    public synchronized void netejar(String nom) {
        try {
            LocalTime inici = LocalTime.now();
            while (ocupat) {
                wait();
            }
            TimeUnit.SECONDS.sleep(3);
            LocalTime fin = LocalTime.now();
            System.out.println(nom + " ha netejat l'emprovador desde " + inici + " fins " + fin);
            ocupat = true;
            notifyAll(); //notifica als clients que ja es pot utilitzar l'emprovador
        } catch (Exception e) {
        }
    }

    public synchronized void emprovarse(List<String> roba) {
        try {
            LocalTime inici = LocalTime.now();
            while (!ocupat) {
                wait();
            }
            TimeUnit.SECONDS.sleep(roba.size());
            LocalTime fin = LocalTime.now();
            ocupat = false;
            notifyAll();
            System.out.println("Emprovant " + roba + " a l'emprovador desde " + inici + " fins " + fin);
        } catch (Exception e) {
        }
    }

}
