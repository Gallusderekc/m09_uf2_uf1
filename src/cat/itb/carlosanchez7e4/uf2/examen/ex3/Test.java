package cat.itb.carlosanchez7e4.uf2.examen.ex3;

public class Test {
    public static void main(String[] args) {
        Emprovador emp=new Emprovador();
        ClientBotiga c1=new ClientBotiga(emp);
        ClientBotiga c2=new ClientBotiga(emp);
        TreballadorBotiga t1=new TreballadorBotiga("Michael",emp);
        TreballadorBotiga t2=new TreballadorBotiga("Bimba", emp);
        c1.start();
        t2.start();
        c2.start();
        t1.start();
    }
}
