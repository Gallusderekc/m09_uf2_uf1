package cat.itb.carlosanchez7e4.uf2.examen.ex3;

public class TreballadorBotiga extends Thread{
    private String nom;
    private Emprovador emp;

    public TreballadorBotiga(String n, Emprovador e) {
        nom = n;
        emp = e;
    }

    @Override
    public void run() {
        emp.netejar(nom);
    }

}
