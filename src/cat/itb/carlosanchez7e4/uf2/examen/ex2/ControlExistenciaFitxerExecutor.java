package cat.itb.carlosanchez7e4.uf2.examen.ex2;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.concurrent.*;

public class ControlExistenciaFitxerExecutor {
    public static void main(String[] args) {
        ScheduledExecutorService temporitzador = Executors.newScheduledThreadPool(1);

        Scanner in = new Scanner(System.in);
        System.out.println("Fitxer?");
        String origen = in.next();

        Callable<Boolean> buscar = () -> {
            Path source = Paths.get(origen);
            boolean esta= Files.exists(source);
            if(esta) System.out.println("Ha aparegut el fitxer");
            return esta;
        };

        ScheduledFuture<Boolean> q;
        try {
            do {
                q = temporitzador.schedule(buscar, 5, TimeUnit.SECONDS);
                //ALTRES FEINES DEL MAIN...
            } while (!q.get());
            temporitzador.shutdown();
        } catch (Exception e) {

        }
    }
}
