package cat.itb.carlosanchez7e4.uf2.examen.ex1;

public class Test {
    public static void main(String[] args) {
        Thread t1=new Thread(new Feina("Planxar",5.3));
        Thread t2=new Thread(new Feina("Rentar plats",2.5));
        Thread t3=new Thread(new Feina("Cuinar",5.4));

        t1.start();
        t2.start();
        t3.start();
    }
}
