package cat.itb.carlosanchez7e4.uf2.examen.ex1;

public class Feina implements Runnable{

    private String nom;
    private double cost;

    public Feina(String n, double qtt){
        nom=n;
        cost=qtt;
    }

    @Override
    public void run() {
        System.out.println("S'està fent la feina "+nom+ " i costarà "+cost+ " euros");
    }
}
