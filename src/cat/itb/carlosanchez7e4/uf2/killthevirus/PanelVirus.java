package cat.itb.carlosanchez7e4.uf2.killthevirus;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;


public class PanelVirus extends JPanel implements MouseListener {
    private ArrayList<Virus> virus = new ArrayList<Virus>();
    public PanelVirus(){
        addMouseListener(this);
    }

    /**
     *  Funció que afegeix 1 objecte de la classe Virus a la llista virus i després inicia el Thread del virus.
     */
    public void addVirus(){
        Virus v = new Virus(this);
        virus.add(v);
        new Thread(v).start();
    }

    /**
     * Funció per fer apareixer a la pantalla tots els objectes virus.
     *
     * @param g Parametre d'entrada que forma el contenidor.
     */
    public void paint(Graphics g){
        super.paint(g);
        virus.forEach((v)->{v.dibuixarVirus(g);});
        Toolkit.getDefaultToolkit().sync();
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }
    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("(" + e.getX() + "," + e.getY() + ")");
        virus.forEach((v)->{v.matar(e.getX(),e.getY());});
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
