package cat.itb.carlosanchez7e4.uf2.exercices.interficies.ex2;

public class Camisa {
    String model;
    String talla;
    String color;

    public Camisa(String model, String talla, String color) {
        this.model = model;
        this.talla = talla;
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public String getTalla() {
        return talla;
    }

    public String getColor() {
        return color;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString(){
        return String.format("Camisa{" +
                "Model: %s, " +
                "Talla: %s, " +
                "Color: %s}",
                model, talla, color);
    }
}
