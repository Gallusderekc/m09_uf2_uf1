package cat.itb.carlosanchez7e4.uf2.exercices.interficies.ex2;

import java.util.Arrays;
import java.util.List;

public class Ex2 {
    public static void main(String[] args) {

        Camisa c1 = new Camisa("A", "XL", "Groc");
        Camisa c2 = new Camisa("B", "XL", "Vermell");
        Camisa c3 = new Camisa("B", "M", "Blau");
        Camisa c4 = new Camisa("C", "L", "Vermell");
        Camisa c5 = new Camisa("A", "S", "Verd");

        List<Camisa> llista = Arrays.asList(c1, c2, c3, c4, c5);

        System.out.println("\nPer talla XL: ");
        printCaracteristica(llista, (Camisa c) -> c.getTalla().equals("XL"));

        System.out.println("\nPer color Vermell: ");
        printCaracteristica(llista, (Camisa c) -> c.getColor().equals("Vermell"));

        System.out.println("\nPer talla M i color Blau:");
        printCaracteristica(llista, (Camisa c) -> c.getTalla().equals("M") && c.getColor().equals("Blau"));
    }

    private static void printCaracteristica(List<Camisa> llista, filtreCamises filtre){
        for (Camisa c : llista){
            if (filtre.test(c)){
                System.out.println(c.toString());
            }
        }
    }
}

@FunctionalInterface
interface filtreCamises{
    boolean test(Camisa c);
}
