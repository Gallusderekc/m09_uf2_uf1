package cat.itb.carlosanchez7e4.uf2.exercices.threads.vendadeentrades;

import java.util.Scanner;

public class VendaDeEntrades {
    public static final Scanner lector = new Scanner(System.in);
    public static Thread[] threads = new Thread[7];
    private static String nombre;
    private static int numEntradas;

    public static void main(String[] args) {

        Runnable usuario1 = Taquilla.reservarEntradas("Lola21", 2);
        Runnable usuario2 = Taquilla.reservarEntradas("Pepito2", 5);
        Runnable usuario3 = Taquilla.reservarEntradas("Juan34", 4);
        Runnable usuario4 = Taquilla.reservarEntradas("Maria23", 1);
        Runnable usuario5 = Taquilla.reservarEntradas("Marta2", 3);
        Runnable usuario6 = Taquilla.reservarEntradas("BaleadasLover", 3);
        Runnable usuario7 = Taquilla.reservarEntradas("Idk555", 3);
        Thread t1 = new Thread(usuario1);
        Thread t2 = new Thread(usuario2);
        Thread t3 = new Thread(usuario3);
        Thread t4 = new Thread(usuario4);
        Thread t5= new Thread(usuario5);
        Thread t6= new Thread(usuario6);
        Thread t7= new Thread(usuario7);


        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
    }


    public static int leerTeclado() {
        Scanner lector = new Scanner(System.in);
        int entero = 0;
        boolean error = false;
        while (!error) {
            error = lector.hasNextFloat();
            if (error) {
                entero = lector.nextInt();
            } else {
                System.out.println("Error al leer digito, introduzcalo de nuevo: ");
                lector.next();
            }
        }
        lector.nextLine();
        return entero;
    }
}