package cat.itb.carlosanchez7e4.uf2.exercices.threads.ex2;

public class EsPrimerThread extends Thread{
    long l;

    public EsPrimerThread(long l) {
        this.l = l;
    }

    @Override
    public void run() {
        printPrimer(l);
    }

    private static void printPrimer(long l) {
        if (esPrimer(l)){
            System.out.println("El nombre " + l + " és primer i el següent nombre primer és " + nextPrimer(l));
        } else {
            System.out.println("El nombre " + l + " no és primer i el següent nombre primer és " + nextPrimer(l));
        }
    }

    public static boolean esPrimer(long l) {
        int counter = 2;
        boolean primer=true;

        while ((primer) && (counter!=l)){
            if (l % counter == 0)
                primer = false;
            counter++;
        }

        return primer;
    }

    public static long nextPrimer(long l) {
        long value = l + 1;
        while (!esPrimer(value)){
            value++;
        }

        return value;
    }
}
