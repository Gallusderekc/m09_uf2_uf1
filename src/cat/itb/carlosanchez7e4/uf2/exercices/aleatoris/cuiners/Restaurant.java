package cat.itb.carlosanchez7e4.uf2.exercices.aleatoris.cuiners;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class Restaurant {
    public static void main(String[] args) {
        BlockingQueue<String> cua = new LinkedBlockingQueue<>();

        ExecutorService ex = Executors.newCachedThreadPool();

        ex.submit(new Cuiner(cua));
        ex.submit(new Cambrer(cua));
        ex.submit(new Cambrer(cua));
    }
}
