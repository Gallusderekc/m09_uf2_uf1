package cat.itb.carlosanchez7e4.uf2.exercices.aleatoris;

public class Nevera {
    public String marcaNevera;
    public int cantidadHuevos;
    public double precio;
    public boolean oferta;

    public Nevera(String marcaNevera, int cantidadHuevos, double precio, boolean oferta) {
        this.marcaNevera = marcaNevera;
        this.cantidadHuevos = cantidadHuevos;
        this.precio = precio;
        this.oferta = oferta;
    }

    public String getMarcaNevera() {
        return marcaNevera;
    }

    public void setMarcaNevera(String marcaNevera) {
        this.marcaNevera = marcaNevera;
    }

    public int getCantidadHuevos() {
        return cantidadHuevos;
    }

    public void setCantidadHuevos(int cantidadHuevos) {
        this.cantidadHuevos = cantidadHuevos;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public boolean isOferta() {
        return oferta;
    }

    public void setOferta(boolean oferta) {
        this.oferta = oferta;
    }

    @Override
    public String toString() {
        return "Nevera{" +
                "marcaNevera='" + marcaNevera + '\'' +
                ", cantidadHuevos=" + cantidadHuevos +
                ", precio=" + precio +
                ", oferta=" + oferta +
                '}';
    }
}