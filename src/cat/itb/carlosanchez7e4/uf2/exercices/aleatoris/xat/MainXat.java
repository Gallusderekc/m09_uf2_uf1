package cat.itb.carlosanchez7e4.uf2.exercices.aleatoris.xat;

public class MainXat {
    public static void main(String[] args) {
        Xat c=new Xat();
        Runnable u1 = new UsuariPregunta(c);
        Runnable u2 = new UsuariResposta(c);
        Thread t1 = new Thread(u1);
        Thread t2 = new Thread(u2);
        t1.start();
        t2.start();
    }
}