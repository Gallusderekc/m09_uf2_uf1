package cat.itb.carlosanchez7e4.uf2.exercices.aleatoris.xat;

public class Xat {
    public String question;
    public String answer;
    private boolean asked = false;

    public synchronized void pregunta(String msg){
        while(asked){
            try{
                wait();
            }catch(InterruptedException e) {
                e.printStackTrace();
            }

        }
        asked = true;
        question = msg;
        notify();
    }

    public synchronized void resposta(String msg){
        while(!asked){
            try{
                wait();
            }catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
        asked = false;
        answer = msg;
        notify();
    }
}