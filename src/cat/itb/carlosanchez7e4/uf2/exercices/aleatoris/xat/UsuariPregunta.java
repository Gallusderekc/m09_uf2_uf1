package cat.itb.carlosanchez7e4.uf2.exercices.aleatoris.xat;

import java.util.Random;

public class UsuariPregunta implements Runnable {
    private Xat c;
    private String[] preguntes = {"a?", "b?", "c?"};

    public UsuariPregunta(Xat c) {
        this.c = c;
    }

    @Override
    public void run() {
        while (c.question == null){
            c.pregunta(preguntes[new Random().nextInt(preguntes.length)]);
            System.out.println(c.question);
        }
    }
}
