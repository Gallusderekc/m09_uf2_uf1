package cat.itb.carlosanchez7e4.uf2.exercices.aleatoris.xat;

import java.util.Random;

public class UsuariResposta implements Runnable {
    private Xat c;
    private String[] respostes = {"Si", "No"};

    public UsuariResposta(Xat c) {
        this.c = c;
    }

    @Override
    public void run() {
        while (c.answer == null){
            c.resposta(respostes[new Random().nextInt(respostes.length)]);
            System.out.println(c.answer);
        }
    }
}
