package cat.itb.carlosanchez7e4.uf2.exercices.callables;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.*;

public class Exercici3 {

    public static void main(String[] args) {
        //Carpeta on calculat tamany
        File fitxer=new File(".");

        ExecutorService ex = Executors.newCachedThreadPool();
        SumFitxer sF = new SumFitxer(fitxer,ex);
        Future<Long> f1 = ex.submit(sF);
        try {
            System.out.println("Tamany: "+f1.get()+" Bytes");
            ex.shutdown();
        } catch (InterruptedException | ExecutionException ignored){

        }
    }
}
class SumFitxer implements Callable<Long> {
    private File fitxer;
    private ExecutorService ex;

    public SumFitxer(File fitxer, ExecutorService ex) {
        this.fitxer = fitxer;
        this.ex = ex;
    }

    public long sumarfitxers() {
        int suma=0;
        if (fitxer.isDirectory()){
            File[] fitxers=fitxer.listFiles();
            ArrayList<Future<Long>> resultats= new ArrayList<>();
            for (File f1:fitxers) {
                SumFitxer sF = new SumFitxer(f1,ex);
                resultats.add(ex.submit(sF));
            }
            try {
                for (Future<Long> resultat:resultats) {
                    suma+=resultat.get();
                }
            } catch (InterruptedException | ExecutionException ignored) {
            }
        }else{
            suma+=fitxer.length();
        }
        return suma;
    }

    @Override
    public Long call() throws Exception {
        return sumarfitxers();
    }
}
