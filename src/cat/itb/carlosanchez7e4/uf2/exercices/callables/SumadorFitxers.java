package cat.itb.carlosanchez7e4.uf2.exercices.callables;

import java.io.File;
import java.util.Objects;
import java.util.concurrent.Callable;

public class SumadorFitxers implements Callable<Long> {

    private long mida;
    private File ruta;

    public SumadorFitxers(File ruta) {
        this.ruta = ruta;
    }

    @Override
    public Long call() throws Exception {
        return sumar(ruta);
    }

    public long sumar(File file) {
            if (file.isDirectory()) {
                File[] continguts = file.listFiles();
                for (File f : continguts) {
                    if (f.isFile()) {
                        mida += f.length();
                    }
                    else mida += sumar(f);
                    }
                }
            else mida+=file.length();
        return mida;
    }
}