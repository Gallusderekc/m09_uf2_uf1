package cat.itb.carlosanchez7e4.uf2.exercices.callables;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TestSumadorFitxers {
    public static void main(String[] args) {

        File inici = new File("/");
        ArrayList<SumadorFitxers> threads= new ArrayList<SumadorFitxers>();

        File[] files = inici.listFiles();
        for(File f: files){
            threads.add(new SumadorFitxers(f));
        }

        ExecutorService ex = Executors.newFixedThreadPool(threads.size());
        ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();

        for(SumadorFitxers sf: threads ){
            futures.add(ex.submit(sf));
        }

        long suma=0;
        for(Future<Long> ff: futures){
            try {
                suma=suma+ff.get();

            }catch(ExecutionException | InterruptedException e){}
        }
        ex.shutdown();
        System.out.println("El total de bytes per la ruta introduïda éss: "+suma);

    }
}