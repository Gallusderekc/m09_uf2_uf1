package cat.itb.carlosanchez7e4.uf2.exercices.callables;

import java.util.concurrent.Callable;

public class SumarFilaCallable implements Callable<Integer> {
    private int []fila;

    public SumarFilaCallable(int[] fila) {
        this.fila = fila;
    }

    @Override
    public Integer call(){
        int sumaFila=0;
        for(int i=0;i<fila.length;i++){
            sumaFila=sumaFila+fila[i];
        }
        return sumaFila;
    }
}
