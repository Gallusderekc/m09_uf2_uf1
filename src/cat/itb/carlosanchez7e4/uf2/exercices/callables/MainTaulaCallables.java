package cat.itb.carlosanchez7e4.uf2.exercices.callables;

import java.util.ConcurrentModificationException;
import java.util.Scanner;
import java.util.concurrent.*;

public class MainTaulaCallables {
    public static void main(String[] args) {
        int [][]taula=omplirTaula();

        //es crea un pool de 3 working threads, per exemple
        ExecutorService ex = Executors.newFixedThreadPool(3);
        //es creen tants callables com files
        SumarFilaCallable []sumaFiles=new SumarFilaCallable[taula.length];
        //es creen tants Futures com Callables
        Future<Integer>[] resultats=new Future[taula.length];
        try {
            int total = 0;
            for (int i = 0; i < taula.length; i++) {
                sumaFiles[i] = new SumarFilaCallable(taula[i]);
                resultats[i] = ex.submit(sumaFiles[i]);
            }
             for(int i=0;i<resultats.length;i++){
                int sumaFila=resultats[i].get();
                System.out.println("Cada fila suma "+sumaFila);
                total = total + resultats[i].get();
            }


            ex.shutdown();
            System.out.println("Suma de totes les files es "+total);
        }catch (InterruptedException | ExecutionException e){}



    }

    public static int[][] omplirTaula (){
        Scanner entrada=new Scanner(System.in);
        System.out.println("Files?");
        int M=entrada.nextInt();
        System.out.println("Columnes?");
        int N =entrada.nextInt();
        int [][]taula=new int[M][N];
        ThreadLocalRandom tlr= ThreadLocalRandom.current();
        for(int i=0;i<M;i++){
            for(int j=0;j<N;j++){
                taula[i][j]=tlr.nextInt(10);
                System.out.print(taula[i][j]+" ");
            }
            System.out.println();
        }
        return taula;
    }

}
