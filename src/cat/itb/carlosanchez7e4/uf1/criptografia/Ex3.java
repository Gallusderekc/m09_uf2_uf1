package cat.itb.carlosanchez7e4.uf1.criptografia;

public class Ex3 {
    public static void main(String[] args) {
        String msg = "Bona tarda i bona sort";
        String msg2 = "12 33 31 11 73 41 11 36 15 11 73 11 73 41 33 41 22 33 27 76";
        char polibi [][] = {
                {'A', 'B', 'C', 'Ç', 'D', 'E', 'F'},
                {'G', 'H', 'I', 'J', 'K', 'L', 'M'},
                {'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S'},
                {'T', 'U', 'V', 'W', 'X', 'Y', 'Z'},
                {'0', '1', '2', '3', '4', '5', '6'},
                {'7', '8', '9', '-', '+', '*', '/'},
                {'.', ',', ' ', '?', '¿', '!', '¡'}
        };

        System.out.println(msg);
        String cipher = encrypt(msg, polibi);
        System.out.println(cipher);
        System.out.println();
        System.out.println(msg2);
        String decipher = decrypt(msg2, polibi);
        System.out.println(decipher);
    }

    private static String decrypt(String msg, char[][] polibi) {
        String[] cadena = msg.split(" ");
        String txt = "";
        for (String coord : cadena){
            int i = Character.getNumericValue(coord.charAt(0) - 1);
            int j = Character.getNumericValue(coord.charAt(1) - 1);
            char ch = polibi[i][j];
            txt += ch;
        }
        return txt;
    }

    private static String encrypt(String msg, char[][] polibi) {
        String txt = "";
        int length = msg.length();
        for (int i = 0; i < length; i++) {
            char ch = msg.charAt(i);
            if (Character.isLowerCase(ch)){
                ch = Character.toUpperCase(ch);
            }
            String coord = "";
            for (int j = 0; j < polibi.length; j++){
                for (int k = 0; k < polibi[j].length; k++){
                    if (ch == polibi[j][k]){
                        coord += j + 1;
                        coord += k + 1;
                    }
                }
            }
            txt += coord;
            txt += " ";
        }
        return txt;
    }

}
