package cat.itb.carlosanchez7e4.uf1.criptografia;

import java.util.Arrays;

public class Ex4 {
    public static void main(String[] args) {
        String msg = "Asrssnfat tlasocu toec sd tg ret ocs otserrepw ar carhawc ameym v aoi iohcsal et prs si";
        int i = 4; // key
        int j = msg.length();
        while (true){
            if (j % i == 0){
                j = j / i;
                break;
            } else {
                j++;
            }
        }

        String decipher = decrypt(msg, i, j);
        System.out.println(decipher);
    }

    private static String decrypt(String msg, int files, int columnes) {
        char[][] matrix = new char[files][columnes];
        int index = 0;
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    if (index < msg.length()) {
                        matrix[i][j] = msg.charAt(index);
                        index++;
                    } else {
                        matrix[i][j] = ' ';
                    }
                }
            }
        for (char[] row : matrix){
            System.out.println(Arrays.toString(row));
        }
        System.out.println();

        String txt = "";
        for (int i = 0; i < columnes; i++){
            for (int j = 0; j < files; j++){
                txt += matrix[j][i];
            }
        }
        return txt;
    }
}
